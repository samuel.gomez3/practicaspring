package com.pragma.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.models.Image;
import com.pragma.repository.IImageRepository;
import com.pragma.service.IImageService;

@Service
public class ImageServiceImpl implements IImageService {

	@Autowired
	private IImageRepository iImageR;

	@Override
	public Image save(Image image) {
		return iImageR.insert(image);
	}

	@Override
	public Image update(Image image) {
		return iImageR.save(image);

	}

	@Override
	public void delete(String id) {
		iImageR.deleteById(id);

	}

	@Override
	public List<Image> findAll() {
		return iImageR.findAll();
	}

	@Override
	public Image findById(String id) {
		return iImageR.findById(id).orElse(null);
	}

	@Override
	public Image findByNumeroIdentificacion(String numeroIdentificacion) {
		return iImageR.findByNumeroIdentificacion(numeroIdentificacion);
	}

	@Override
	public boolean isExist(Image image) {
		return findByNumeroIdentificacion(image.getNumeroIdentificacion()) != null;
	}

}
