package com.pragma.rest.controller;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;


import com.pragma.models.Image;
import com.pragma.service.IImageService;



@RestController
@RequestMapping("/image")
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
public class ImageRestController {

	@Autowired
	private IImageService iImageService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Image>> listAllImages() { // list all "Clientes"
		List<Image> images = iImageService.findAll();
		if (images.isEmpty())
			return new ResponseEntity<List<Image>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Image>>(images, HttpStatus.OK);
	}


	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Image> getImageById(@PathVariable("id") String id) { // gets "Cliente" for the "id".
		Image images = iImageService.findById(id);
		if (images == null)
			return new ResponseEntity<Image>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<Image>(images, HttpStatus.OK);
	}

	@RequestMapping(value = "/numeroIdentificacion/{numeroIdentificacion}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Image> getImageByNumeroIdentificacion( // gets "Cliente" for the "numeroIdentificacion".
			@PathVariable("numeroIdentificacion") String numeroIdentificacion) {
		Image images = iImageService.findByNumeroIdentificacion(numeroIdentificacion);
		if (images == null)
			return new ResponseEntity<Image>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<Image>(images, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> createImage(@RequestParam("image") MultipartFile image,
			@RequestParam("numeroIdentificacion") String numeroIdentificacion, HttpServletRequest request,
			UriComponentsBuilder uriComponentsBuilder) {
		Image images = new Image();
		try {
			images.setImagen(Base64.getEncoder().encodeToString(image.getBytes()));
		} catch (IOException e) {

			e.printStackTrace();
		}
		images.setNumeroIdentificacion(numeroIdentificacion);
		if (iImageService.isExist(images))
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		iImageService.save(images);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/{id}").buildAndExpand(images.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE) // updates customer according to
																												// the attributes entered in the
																												// parameters "cliente" and "id".
	public ResponseEntity<Image> updateImage(@PathVariable("id") String id, @RequestParam("image") MultipartFile image,
			@RequestParam("numeroIdentificacion") String numeroIdentificacion, HttpServletRequest request) {
		
		Image currentimages = iImageService.findById(id);
		if (currentimages == null)
			return new ResponseEntity<Image>(HttpStatus.NOT_FOUND);
		currentimages.setNumeroIdentificacion(numeroIdentificacion);
		try {
			currentimages.setImagen(Base64.getEncoder().encodeToString(image.getBytes()));
		} catch (IOException e) {

			e.printStackTrace();
		}
		iImageService.update(currentimages);
		return new ResponseEntity<Image>(currentimages, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE) // Delete "Cliente" according to the parameter "id".
	public ResponseEntity<Image> deleteImage(@PathVariable("id") String id) {
		Image cliente = iImageService.findById(id);
		if (cliente == null)
			return new ResponseEntity<Image>(HttpStatus.NOT_FOUND);
		iImageService.delete(id);
		return new ResponseEntity<Image>(HttpStatus.NO_CONTENT);
	}




}
