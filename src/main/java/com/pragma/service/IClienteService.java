package com.pragma.service;

import java.util.List;

import com.pragma.models.Cliente;

public interface IClienteService {

	Cliente findById(long id);

	Cliente findByNumeroIdentificacion(String numeroIdentificacion);

	void save(Cliente cliente);

	void update(Cliente cliente);

	void delete(long id);

	List<Cliente> findAll();

	public boolean isExist(Cliente cliente);

	List<Cliente> findByEdad(int edad);

}
