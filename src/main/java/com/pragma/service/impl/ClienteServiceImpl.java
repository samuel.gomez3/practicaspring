package com.pragma.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.models.Cliente;
import com.pragma.repository.IClienteRepository;
import com.pragma.service.IClienteService;

@Service
public class ClienteServiceImpl implements IClienteService {

	@Autowired
	private IClienteRepository clienteR;

	@Override
	public Cliente findById(long id) {
		return (Cliente) clienteR.findById(id).orElse(null); // searches for "Cliente" by Id and if not found returns null
	}

	@Override
	public Cliente findByNumeroIdentificacion(String identificacion) {
		if (identificacion.matches("[+-]?\\d*(\\.\\d+)?")) // checks that the string is a number
			return clienteR.findByNumeroIdentificacion(identificacion);
		else
			return clienteR.findByNumeroIdentificacion(identificacion.substring(2)); // removes the first two characters that refer to the
																						// document type, and searches for "cliente" by the 
																						// document number
	}

	@Override
	public void save(Cliente cliente) {
		clienteR.save(cliente);
	}

	@Override
	public void update(Cliente cliente) {
		clienteR.save(cliente);
	}

	@Override
	public void delete(long id) {
		clienteR.deleteById(id);
	}

	@Override
	public List<Cliente> findAll() {
		return (List<Cliente>) clienteR.findAll();
	}

	@Override
	public boolean isExist(Cliente cliente) {
		return findByNumeroIdentificacion(cliente.getNumeroIdentificacion()) != null; // check if the "Cliente" already
																						// exists
	}

	@Override
	public List<Cliente> findByEdad(int edad) {
		return ((List<Cliente>) clienteR.findAll()).stream().filter(p -> p.getEdad() >= edad).toList(); // Returns a list of "Cliente"
																										// with the attribute "edad"
																										// greater than the parameter "edad".
	}

}
