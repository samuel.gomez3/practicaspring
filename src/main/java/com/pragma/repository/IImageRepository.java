package com.pragma.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.pragma.models.Image;

@Repository
public interface IImageRepository extends MongoRepository<Image, String> {
	public Image findByNumeroIdentificacion(String numeroIdentificacion);

}
