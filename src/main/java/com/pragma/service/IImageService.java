package com.pragma.service;

import java.util.List;

import com.pragma.models.Image;

public interface IImageService {

	Image save(Image image);

	Image update(Image image);

	void delete(String id);
	
	Image findById(String id);

	List<Image> findAll();
	
	Image findByNumeroIdentificacion(String numeroIdentificacion);
	
	public boolean isExist(Image image);


}
