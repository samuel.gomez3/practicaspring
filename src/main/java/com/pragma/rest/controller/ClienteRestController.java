package com.pragma.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.pragma.models.Cliente;
import com.pragma.service.IClienteService;

@RestController
@RequestMapping("/cliente")
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
public class ClienteRestController {

	@Autowired
	private IClienteService clienteService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Cliente>> listAllClientes() { // list all "Clientes"
		List<Cliente> clientes = clienteService.findAll();
		if (clientes.isEmpty())
			return new ResponseEntity<List<Cliente>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Cliente>>(clientes, HttpStatus.OK);
	}

	@RequestMapping(value = "/edad/{edad}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<List<Cliente>> listClientesByEdad(@PathVariable("edad") int edad) { // list all "Cliente" with "edad" attribute
																								// greater than the entered "edad" parameter
		List<Cliente> clientes = clienteService.findByEdad(edad);
		if (clientes.isEmpty())
			return new ResponseEntity<List<Cliente>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Cliente>>(clientes, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> getClienteById(@PathVariable("id") long id) { // gets "Cliente" for the "id".
		Cliente cliente = clienteService.findById(id);
		if (cliente == null)
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
	}

	@RequestMapping(value = "/numeroIdentificacion/{numeroIdentificacion}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> getClienteByNumeroIdentificacion( // gets "Cliente" for the "numeroIdentificacion".
			@PathVariable("numeroIdentificacion") String numeroIdentificacion) {
		Cliente cliente = clienteService.findByNumeroIdentificacion(numeroIdentificacion);
		if (cliente == null)
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> createCliente(@Valid @RequestBody Cliente cliente, // creates a customer according to
																					// the attributes entered in the
																					// "cliente" parameter.
			UriComponentsBuilder uriComponentsBuilder) {
		if (clienteService.isExist(cliente))
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		clienteService.save(cliente);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/{id}").buildAndExpand(cliente.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE) // updates customer according to
																												// the attributes entered in the
																												// parameters "cliente" and "id".
	public ResponseEntity<Cliente> updateCliente(@PathVariable("id") long id, @RequestBody Cliente cliente) {
		Cliente currentCliente = clienteService.findById(id);
		if (currentCliente == null)
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		currentCliente.setApellidos(cliente.getApellidos());
		currentCliente.setCiudadNacimiento(cliente.getCiudadNacimiento());
		currentCliente.setEdad(cliente.getEdad());
		currentCliente.setNombres(cliente.getNombres());
		currentCliente.setNumeroIdentificacion(cliente.getNumeroIdentificacion());
		currentCliente.setTipoIdentificacion(cliente.getTipoIdentificacion());
		clienteService.update(currentCliente);
		return new ResponseEntity<Cliente>(currentCliente, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE) // Delete "Cliente" according to the parameter "id".
	public ResponseEntity<Cliente> deleteCliente(@PathVariable("id") long id) {
		Cliente cliente = clienteService.findById(id);
		if (cliente == null)
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		clienteService.delete(id);
		return new ResponseEntity<Cliente>(HttpStatus.NO_CONTENT);
	}

}
