package com.pragma.repository;

import org.springframework.data.repository.CrudRepository;

import com.pragma.models.Cliente;

public interface IClienteRepository extends CrudRepository<Cliente, Long> {
	
	Cliente findByNumeroIdentificacion(String numeroIdentificacion);

}
