package com.pragma.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Data
public class Cliente {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Size(min = 2, max = 60)
	private String nombres;
	@Size(min = 2, max = 60)
	private String apellidos;
	@Size(min = 2, max = 60)
	private String tipoIdentificacion;
	@Size(min = 8, max = 12)
	private String numeroIdentificacion;
	@Min(13)
	@Max(99)
	private int edad;
	@Size(min = 2, max = 60)
	private String ciudadNacimiento;

}
